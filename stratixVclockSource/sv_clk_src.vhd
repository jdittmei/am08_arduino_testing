library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-- CLKOUT0 gives 500 MHz
-- CLKOUT  gives 450 MHz (for comparison!)
-- USER_PB1 is reset

-- DOES NOT WORK AS INTENDED... CLKOUT gives    | 500 MHz | 525 MHz | 450 MHz | 475 MHz |
-- LED_G turned on |    0    |    1    |    2    |    3    |
-- USER_PB0 is to select CLKOUT

entity sv_clk_src is
	port
	(
		-- Input ports
		CLKIN_50	: in  std_logic;	-- 50.000 MHz
		USER_PB0	: in 	std_logic;	-- When you press and hold down the push button, the device pin is set to logic 0
		USER_PB1	: in 	std_logic;	-- When you press and hold down the push button, the device pin is set to logic 0
		USER_LED_G: out std_logic_vector(4 downto 0); -- User-Defined LEDs. Driving a logic 0 on the I/O port turns the LED ON
		USER_LED_R: out std_logic_vector(4 downto 0); -- User-Defined LEDs. Driving a logic 0 on the I/O port turns the LED ON
		CLKOUT    : out std_logic;	
		CLKOUT0   : out std_logic	
	);
end sv_clk_src;

architecture rtl of sv_clk_src is

	component pll is
		port (
			refclk   : in  std_logic := 'X'; -- clk
			rst      : in  std_logic := 'X'; -- reset
			outclk_0 : out std_logic;        -- clk
			locked   : out std_logic         -- export
		);
	end component pll;
	
	component pll_525 is
		port (
			refclk   : in  std_logic := 'X'; -- clk
			rst      : in  std_logic := 'X'; -- reset
			outclk_0 : out std_logic;        -- clk
			locked   : out std_logic         -- export
		);
	end component pll_525;
	
	component pll_550 is
		port (
			refclk   : in  std_logic := 'X'; -- clk
			rst      : in  std_logic := 'X'; -- reset
			outclk_0 : out std_logic;        -- clk
			locked   : out std_logic         -- export
		);
	end component pll_550;
	
	component pll_450 is
		port (
			refclk   : in  std_logic := 'X'; -- clk
			rst      : in  std_logic := 'X'; -- reset
			outclk_0 : out std_logic;        -- clk
			locked   : out std_logic         -- export
		);
	end component pll_450;	

	component pll_475 is
		port (
			refclk   : in  std_logic := 'X'; -- clk
			rst      : in  std_logic := 'X'; -- reset
			outclk_0 : out std_logic;        -- clk
			locked   : out std_logic         -- export
		);
	end component pll_475;	
	
	component clkctrl is
		port (
			inclk3x   : in  std_logic                    := 'X';             -- inclk3x
			inclk2x   : in  std_logic                    := 'X';             -- inclk2x
			inclk1x   : in  std_logic                    := 'X';             -- inclk1x
			inclk0x   : in  std_logic                    := 'X';             -- inclk0x
			clkselect : in  std_logic_vector(1 downto 0) := (others => 'X'); -- clkselect
			outclk    : out std_logic                                        -- outclk
		);
	end component clkctrl;

	
	
signal clks : std_logic_vector(4 downto 0);

signal counter : std_logic_vector(27 downto 0) := (others => '0');
signal selector: std_logic_vector(1 downto 0) := "00";-- range 0 to 4 := 0;

type fsm_type is (idle, counting);
signal state : fsm_type;

signal clkint0 : std_logic;
signal clkint1 : std_logic;

begin


CLKOUT0  <= clks(0);
CLKOUT   <= clks(3);

--	i_ctrl: clkctrl 
--		port map(
--			inclk3x   => clks(1),
--			inclk2x   => clks(0),
--			inclk1x   => '0',--clks(1),
--			inclk0x   => '0',--clks(0),
--			clkselect => selector(1 downto 0),
--			outclk    => clkint0
--		);
--		
--	i_ctrl1: clkctrl 
--		port map(
--			inclk3x   => clks(4),
--			inclk2x   => clks(3),
--			inclk1x   => '0',--clks(1),
--			inclk0x   => '0',--clks(0),
--			clkselect => selector(1 downto 0),
--			outclk    => clkint1
--		);
--		
--	i_ctrl2: clkctrl 
--		port map(
--			inclk3x   => clkint1,--'0',--clks(4),
--			inclk2x   => clkint0,--'0',--clks(3),
--			inclk1x   => '0',--(1),
--			inclk0x   => '0',--clkint0,--(0),
--			clkselect => selector(1 downto 0),
--			outclk    => CLKOUT
--		);	


	process(CLKIN_50)
	begin
		if rising_edge(CLKIN_50) then
			if(USER_PB1 = '0')then	-- reset!
				counter 	<= (others => '0');
				selector <= "00";
				state		<= idle;
			else
				case state is
					when idle =>
						if(USER_PB0 = '0')then
							selector <= std_logic_vector(unsigned(selector) + 1);
							counter  <= x"1000000";
							state    <= counting;
						end if;
					
					when counting =>
						if(counter = x"0000001")then
							if(USER_PB0 = '1')then
								counter 	<= x"0000000";
								state 	<= idle;
							end if;
						else
							counter <= std_logic_vector(unsigned(counter) - 1);
						end if;
					
					when others =>
						counter 	<= (others => '0');
						state		<= idle;
						
				end case;
			end if;
			USER_LED_G 				<= (others => '1');
			USER_LED_G(to_integer(unsigned(selector))) <= '0';
		end if;		
	end process;


	i_pll_500: pll
		port map(
			refclk   => CLKIN_50,
			rst      => not USER_PB1,
			outclk_0 => clks(0),
			locked   => USER_LED_R(0)
		);
		
	i_pll_525: pll_525
		port map(
			refclk   => CLKIN_50,
			rst      => not USER_PB1,
			outclk_0 => clks(1),
			locked   => USER_LED_R(1)
		);
		
	i_pll_550: pll_550
		port map(
			refclk   => CLKIN_50,
			rst      => not USER_PB1,
			outclk_0 => clks(2),
			locked   => USER_LED_R(2)
		);
		
	i_pll_450: pll_450
		port map(
			refclk   => CLKIN_50,
			rst      => not USER_PB1,
			outclk_0 => clks(3),
			locked   => USER_LED_R(3)
		);
		
		
	i_pll_475: pll_475
		port map(
			refclk   => CLKIN_50,
			rst      => not USER_PB1,
			outclk_0 => clks(4),
			locked   => USER_LED_R(4)
		);		
end rtl;
