#ifndef AM08_H
#define AM08_H

// SPI settings
#define AM08_SPI_MAXSPEED 25000000
#define AM08_SPI_MSBFIRST MSBFIRST
#define AM08_SPI_MODE     SPI_MODE3 // checked with Code: actually looks like Mode3!
/* from AM08 specs, page 36:
The SPI data transfer operation is depicted in Fig. 15 for
different CPHA and CPOL modes. For the AM08 implementation, CPHA=CPOL=0 mode
will be used. This means that the MOSI line will be sampled at the rising edge of the SPI
clock, while the MISO signal will be set at the falling edge. The SPI clock frequency will
be 25 MHz
*/


// SPI OP Codes
const byte READ     = 0b01000000; // AM08's read command
const byte WRITE    = 0b00000000; // AM08's write command

// SPI Acknowledge Byte and response bytes after write and read command
const byte ACK      = 0b10101100; // first word returned after write and read command
const byte RESPONSE = 0b00000001; // followed by four of these if it was a write command!
// otherwise returns data, MSByte first

// we have 6 bit addresses
const byte ADDRESS_MASK = 0b00111111;

//address table
// maybe this does not need to be on the Arduino, but can be part of software on host!!
// on page 0
const byte SPI_ADR_DeviceId       = 0x00;
const byte SPI_ADR_PageNo         = 0x01;
const byte SPI_ADR_Thr            = 0x02;
const byte SPI_ADR_State          = 0x03;
const byte SPI_ADR_EnableBlock    = 0x04;
const byte SPI_ADR_HoldIn         = 0x05;
const byte SPI_ADR_ContinuousMode = 0x06;
const byte SPI_ADR_ForceRead      = 0x07;
const byte SPI_ADR_DisableThis    = 0x08;
const byte SPI_ADR_Layer          = 0x09;
const byte SPI_ADR_DontCare       = 0x0A;
const byte SPI_ADR_DisableRead    = 0x0B;
const byte SPI_ADR_AmWeRe         = 0x0E;
const byte SPI_ADR_AmAddress      = 0x0F;
const byte SPI_ADR_AmData0        = 0x10;
const byte SPI_ADR_AmData1        = 0x11;
const byte SPI_ADR_AmData2        = 0x12;
const byte SPI_ADR_AmData3        = 0x13;
const byte SPI_ADR_AmData4        = 0x14;
const byte SPI_ADR_AmData5        = 0x15;
const byte SPI_ADR_AmData6        = 0x16;
const byte SPI_ADR_AmData7        = 0x17;
const byte SPI_ADR_ClkrDivider    = 0x18;
const byte SPI_ADR_InitEvent      = 0x19;
const byte SPI_ADR_ReadEvent      = 0x1A;
const byte SPI_ADR_Equalizer      = 0x1B; //test this -> set to 0/1
const byte SPI_ADR_AutoEqua       = 0x1C; //test this -> set to 0/1
const byte SPI_ADR_RoadRead       = 0x1D;

// on page 1
const byte SPI_ADR_GeoAdd         = 0x02;
const byte SPI_ADR_RxdSnapshotEn  = 0x03;
const byte SPI_ADR_RxdSnapshot31  = 0x04;
const byte SPI_ADR_RxdSnapshot63  = 0x05;
const byte SPI_ADR_RxdSnapshot65  = 0x06;
const byte SPI_ADR_RxdErrCntReset = 0x07;
const byte SPI_ADR_RxdCtrlErrCnt  = 0x08;
const byte SPI_ADR_RxcCoreErrCnt  = 0x09;
const byte SPI_ADR_RoadDecoderEn  = 0x0A;
const byte SPI_ADR_FifoOrRffs     = 0x0B;

// on page 2
const byte SPI_ADR_IsClkhDephased 		  = 0x02; // test this -> set to 0/1
const byte SPI_ADR_ClkfSel              = 0x03;
const byte SPI_ADR_ClkfdaisySel         = 0x04;
const byte SPI_ADR_DpllClkOutSelector   = 0x07;

// on page 3
const byte SPI_ADR_BistOffset 		= 0x02;
const byte SPI_ADR_BistLblock 		= 0x03;
const byte SPI_ADR_BistEnable 		= 0x04;
const byte SPI_ADR_BistStart 		  = 0x05;
const byte SPI_ADR_BistSkipclean 	= 0x06;
const byte SPI_ADR_BistReset 		  = 0x07;
const byte SPI_ADR_BistDone 		  = 0x08;
const byte SPI_ADR_BistCRC 			  = 0x09;
const byte SPI_ADR_PrbsBusout     = 0x0A;
const byte SPI_ADR_PrbsBusIn      = 0x0B;
const byte SPI_ADR_PrbsRoadOut    = 0x0C;
const byte SPI_ADR_PrbsRoadInb    = 0x0D;
const byte SPI_ADR_PrbsErrors     = 0x0E;
// on page 4 - to be added
// on page 5 - to be added

#endif // AM08_H
