# AM08 Arduino Testing

This repository contains the code for an Arduino UNO used to perform first tests of the Associative Memory (AM) ASIC version 08.

### References
[Arduino SPI library](https://www.arduino.cc/en/reference/SPI)

[Arduino Serial Communication](https://www.arduino.cc/reference/en/language/functions/communication/serial/)

[AM08 source code repository](https://gitlab.cern.ch/am-asic/am-asic-am08)

[Associative Memory ASIC Specifications](https://cds.cern.ch/record/2320701)
