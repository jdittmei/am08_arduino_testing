// Author:  Sebastian Dittmeier
// Date:    07.09.2021
// E-mail:  sebastian.dittmeier@cern.ch

#include <SPI.h>    // the AM08 communicates using SPI
#include "AM08.h"   // holds register list and device specific commands
#include "device.h"

int incomingByte = 0; // for incoming serial data
int digiRead = 0;
int anaRead  = 0;
float current1V0  = 0;
float current1V8  = 0;
float voltage1V0  = 0;
float voltage1V8  = 0;

// the setup function runs once when you press reset or power the board
void setup() 
{
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);  
  // SPI by the user
  pinMode(chipSelectPin, OUTPUT);
  pinMode(ss_slavePin, OUTPUT);  // see comment below SPI.begin();
  //pinMode(spi_sckPin, OUTPUT);
  //pinMode(spi_mosiPin, OUTPUT);
  //pinMode(spi_misoPin, INPUT);
  // remaining pins
  pinMode(chipResetPin, OUTPUT);
  pinMode(chipHoldInPin, OUTPUT);
  pinMode(chipHoldOutPin, INPUT);
  pinMode(chipIsSyncPin, INPUT);
  // Analog inputs
  pinMode(voltage1V0Pin, INPUT);
  pinMode(voltage1V8Pin, INPUT);
  pinMode(current1V0Pin, INPUT);
  pinMode(current1V8Pin, INPUT);

  // initialize the pins
  digitalWrite(LED_BUILTIN, LOW);     // turn LED off first thing at start
  digitalWrite(chipSelectPin, HIGH);  // Raise ChipSelect, no SPI ongoing
  digitalWrite(ss_slavePin, HIGH);    // Raise Arduino Slave Pin (should not be used any further)
  digitalWrite(chipResetPin, HIGH);   // Reset the AM08
  digitalWrite(chipHoldInPin, LOW);   // And disable hold in for the AM
  //digitalWrite(spi_sckPin, LOW);   // And disable hold in for the AM
  //digitalWrite(spi_mosiPin, LOW);   // And disable hold in for the AM
  // UART communication
  Serial.begin(9600); // sets data rate to 9600 bps

  // start the SPI library:
  SPI.begin();
  // there is no one else needing the SPI port, so we can just begin transacation and never kill it!
  SPI.beginTransaction(SPISettings(AM08_SPI_MAXSPEED/100., AM08_SPI_MSBFIRST, AM08_SPI_MODE));  // we try it at a slower speed to start with!
  // this command would free the SPI module
  // end Transacation, free SPI: SPI.endTransaction()
  
  // send welcome message and display the menu
  Serial.println("*- Hello to AM08 Testing -*");
  print_menu();

  // setup complete, turn on LED
  digitalWrite(LED_BUILTIN, HIGH);   // turn LED on once setup is completed
}


// the loop function runs over and over again forever
void loop() 
{
  // wait for input
  if (Serial.available() > 0) {
    incomingByte = Serial.parseInt();    
    // say what you got:
    if(incomingByte != 0)
    {
      Serial.print("I received: ");
      Serial.println(incomingByte, DEC);
  
      if(incomingByte == 1) {
        digitalWrite(chipResetPin, HIGH);   // Reset the AM08  
        Serial.println("Set Reset");
      }    
      if(incomingByte == 2) {
        digitalWrite(chipResetPin, LOW);    // Release the Reset
        Serial.println("Released Reset");
      }    
      if(incomingByte == 3) {
        digitalWrite(chipHoldInPin, HIGH);   // Set HoldIn the AM08  
        Serial.println("Set HoldIn");
      }    
      if(incomingByte == 4) {
        digitalWrite(chipHoldInPin, LOW);    // Release the HoldIn
        Serial.println("Released HoldIn");
      }    
      if(incomingByte == 5) {
        read_status();
      }
      if(incomingByte == 6) {
        SPI_write();
      }
      if(incomingByte == 7) {
        SPI_read();
      }
      if(incomingByte == 8) {
        measureCurrents();
      }
      if(incomingByte == 9) {
        toggleChipSelect();
      }
      if(incomingByte == 10) {
        Serial.println(runBIST(), HEX);
        Serial.print("We expect: ");
        Serial.println(0x661da7df, HEX);
      }
      if(incomingByte == 11) {
        writeCheck();
      }
      if(incomingByte == 12) {
        tb_SRAM();
      }
      if(incomingByte == 13) {
        writeAllRnd();
      }
      if(incomingByte == 14) {
        writeBlockSelect();
      }
      if(incomingByte == 15) {
        writeReadCoreC();
      }
      print_menu();
    }
  }
}

void print_menu()
{
  Serial.println("---------------------------");
  Serial.println("Menu:");
  Serial.println("      1: Set Reset");
  Serial.println("      2: Release Reset");
  Serial.println("      3: Set HoldIn");
  Serial.println("      4: Release HoldIn");
  Serial.println("      5: Read Status");  
  Serial.println("      6: SPI write register");  
  Serial.println("      7: SPI read register");  
  Serial.println("      8: Monitor currents");  
  Serial.println("      9: Toggle ChipSelect");
  Serial.println("     10: Run BIST");
  Serial.println("     11: Write register and read check");
  Serial.println("     12: Perform Check: tb_SRAM.sv");
  Serial.println("     13: Write all patterns random data");
  Serial.println("     14: Write only single block");
  Serial.println("     15: Write and read full Core C (0 -> 0s, 1 -> 1s, else random)");
  Serial.println("---------------------------");
  Serial.println("To select State, write register 3");
  Serial.println("0 = idle, 1 = write, 2 = compare, 3 = read");
  Serial.println("To enable blocks, write register 4");
  Serial.println("bit mask, values = 0 to 7");
  Serial.println("---------------------------");
  Serial.println("Press correspdonding number");
}

void toggleChipSelect() 
{
  digitalWrite(chipSelectPin, LOW);
  delay(1000);
  digitalWrite(chipSelectPin, HIGH);  
}

void read_status()
{
  digiRead = digitalRead(chipHoldOutPin);
  Serial.println("-*-*-*-*-*-*-*-*-*-*-*-*-*-");
  Serial.print("HoldOut: ");    
  Serial.print(digiRead, DEC);   
  Serial.print("\n"); 
  digiRead = digitalRead(chipIsSyncPin);
  Serial.print("IsSync:  ");  
  Serial.print(digiRead, DEC);   
  Serial.print("\n");
  Serial.println("-*-*-*-*-*-*-*-*-*-*-*-*-*-");
  
}

void SPI_write()
{
  Serial.println("-^-^-^-^-^-^-^-^-^-^-^-^-^-");
  Serial.println("Register address?");
  int address;
  while(Serial.available() == 0){}
  address = Serial.parseInt();  
  Serial.println("Register value?");
  int value;
  while(Serial.available() == 0){}
  value = Serial.parseInt();  
  Serial.print("Writing register ");
  Serial.print(address, HEX);
  Serial.print(" with value ");
  Serial.println(value, HEX);
  value = writeRegister(address, value);
  if(value != 0)
  {
    Serial.print("ERROR: Return value of SPI write: ");
    Serial.println(value, DEC);
  }
  Serial.println("-^-^-^-^-^-^-^-^-^-^-^-^-^-");
}

void SPI_read()
{
  Serial.println("-^-^-^-^-^-^-^-^-^-^-^-^-^-");
  Serial.println("Register address?");
  int address;
  while(Serial.available() == 0){}
  address = Serial.parseInt();  
  Serial.print("Reading register ");
  Serial.println(address, HEX);
  uint32_t value = readRegister(address);
  if(value == -1)
  {
    Serial.print("ERROR: Return value of SPI read: ");
    Serial.println(value, HEX);
  }
  else
  {
    Serial.print("Register value: ");
    Serial.println(value, HEX);    
  }
  Serial.println("-^-^-^-^-^-^-^-^-^-^-^-^-^-");
}

//Read from register from the AM08:
uint32_t readRegister(byte thisRegister)
{

  // address is 6 bits + OPcode in 2 MSBs
  byte dataToSend = (thisRegister & ADDRESS_MASK) | READ;
  byte dataReceived;
  // take the chip select low to select the device:
  digitalWrite(chipSelectPin, LOW);
  
  dataReceived = SPI.transfer(dataToSend); //Send register location
 
  if (dataReceived != ACK) {
   return -1;
  }  

  byte i = 4;
  uint32_t result = 0;
  while (i > 0)
  {
    dataReceived = SPI.transfer(0x00);  //Send value to record into register
    result = result << 8;
    result = result | dataReceived;
    i = i-1;
    //Serial.println(result, HEX);
  }

  // take the chip select high to de-select:
  digitalWrite(chipSelectPin, HIGH);

  return result;
}


//Sends a write command to the AM08
//following Figure 17 of AM08 specs, SPI write protocol
int writeRegister(byte thisRegister, uint32_t thisValue)
{

  // address is 6 bits + OPcode in 2 MSBs
  byte dataToSend = (thisRegister & ADDRESS_MASK) | WRITE;
  byte dataReceived;
  // take the chip select low to select the device:
  digitalWrite(chipSelectPin, LOW);

  dataReceived = SPI.transfer(dataToSend); //Send register location
  
  if (dataReceived != ACK) {
    return 5;
  }
//  else {
//    Serial.println("I received ACK!");
//  }
  
  byte i = 4;
  while (i > 0)
  {
    dataToSend = ((thisValue >> ((i-1)*8)) & 0xFF);
    dataReceived = SPI.transfer(dataToSend);  //Send value to record into register
    if (dataReceived != RESPONSE) 
    {
      return i;
    }
//    else
//    {
//      Serial.println("I received Response!");
//    }
    i = i-1;
  }

  // take the chip select high to de-select:
  digitalWrite(chipSelectPin, HIGH);

  return 0;
}


void measureCurrents()
{
  anaRead = analogRead(current1V0Pin);
  //Serial.println(anaRead);
  //delay(500);
  // and translate voltage into current!
  // there is a 499 Ohm resistor between GND and I_1V0
  // formula for calculation: I_MON = I_OUT / 1000
  // voltage to monitor: U = R * I_MON
  // -> I_MON = U / R --> I_OUT = U/R*1000
  // in good approximation: I_OUT = U * 2
  current1V0 = anaRead*2.0*voltageRange/voltageUnits;

  anaRead = analogRead(current1V8Pin);
  //Serial.println(anaRead);
  //delay(500);
  current1V8 = anaRead*2.0*voltageRange/voltageUnits;

  anaRead = analogRead(voltage1V0Pin);
  //Serial.println(anaRead);
  //delay(500);
  voltage1V0 = anaRead*1.0*voltageRange/voltageUnits;
  anaRead = analogRead(voltage1V8Pin);
  //Serial.println(anaRead);
  //delay(500);
  voltage1V8 = anaRead*1.0*voltageRange/voltageUnits;
  
  Serial.println("-I-I-I-I-I-I-I-I-I-I-I-I-");
  Serial.print("1.0 V: U = ");    
  Serial.print(voltage1V0, DEC); 
  Serial.print(" mV \t I = ");   
  Serial.print(current1V0, DEC);  
  Serial.print(" mA\n");  
  Serial.print("1.8 V: U = ");    
  Serial.print(voltage1V8, DEC); 
  Serial.print(" mV \t I = ");   
  Serial.print(current1V8, DEC);  
  Serial.print(" mA\n");  
  Serial.println("-I-I-I-I-I-I-I-I-I-I-I-I-");
}

// following Testbench from here:
// https://gitlab.cern.ch/am-asic/am-asic-am08/-/blob/fix_pnr28/FrontEnd/simulation/tb_BIST.sv
//        readSPI(00);//read device id
//        
//        // actual test procedure
//        
//        //sendWriteCommandViaRXdec(01, 32'h0000_0001, geo_addr_signal); // write addr 1 data 1 geo 0 -> change to page 1
//        writeSPI(04, 32'h0000_0000);
//        // change to page 2 )
//        //writeSPI(01, 32'h0000_0002);
//        //writeSPI(02, 32'h0000_0001); //clk dephased
//        // change to page 3 (BIST page)
//        writeSPI(18, 2'b10); // clkr /2
//        writeSPI(01, 32'h0000_0003);
//        //readSPI(01, misoMessage); // re-read
//        // let's enable BIST (re-routing in the mux internal to the chip)
//        writeSPI(04, 32'h0000_0001);
//        //reset BIST
//        writeSPI(07, 32'h0000_0001);
//        writeSPI(07, 32'h0000_0000);
//        //wnable block 1
//        writeSPI(01, 32'h0000_0000);
//        writeSPI(04, 32'h0000_0001); // enable block
//        // start BIST
//        writeSPI(01, 32'h0000_0003);
//        writeSPI(05, 32'h0000_0001);
//        // calibre depending the duration of BIST 
//        #(16ms)
//        readSPI(09); // AM_DATA_0

// going for procedure in tb_BIST_coreA.sv
 //sendWriteCommandViaRXdec(01, 32'h0000_0001, geo_addr_signal); // write addr 1 data 1 geo 0 -> change to page 1
//        writeSPI(04, 32'h0000_0000);
//        // change to page 2 )
//        //writeSPI(01, 32'h0000_0002);
//        writeSPI(02, 32'h0000_0001); //clk dephased
//        // change to page 3 (BIST page)
//        //writeSPI(18, 2'b10); // clkr /2
//        writeSPI(01, 32'h0000_0003);
//        //readSPI(01, misoMessage); // re-read
//        // let's enable BIST (re-routing in the mux internal to the chip)
//        writeSPI(04, 32'h0000_0001);
//        //reset BIST
//        writeSPI(07, 32'h0000_0001);
//        writeSPI(07, 32'h0000_0000);
//        //wnable block 1
//        writeSPI(01, 32'h0000_0000);
//        writeSPI(04, 32'h0000_0001); // enable block
//        // start BIST
//        writeSPI(01, 32'h0000_0003);
//        writeSPI(05, 32'h0000_0001);
//        // calibre depending the duration of BIST 
//        #(4ms)
//        readSPI(09); // AM_DATA_0

uint32_t runBIST()
{
  int counter = 0;
  uint32_t value = readRegister(SPI_ADR_DeviceId);
  if(value != 0xA550C1A8)
  {
    Serial.println("Error");
    return -1;
  }
  
  value = writeRegister(SPI_ADR_PageNo,0);
  if(value != 0)
  {
    Serial.println("Error");
    Serial.println(value, HEX);
    return counter; //0
  }
  value = readRegister(SPI_ADR_PageNo);
  if (  value != 0)
  {
    Serial.println("Error SPI_ADR_PageNo!");
  }
  
  ///////////////////////////////////////////////////////
  // writeSPI(04, 32'h0000_0000);
  counter = counter + 1;
  value = writeRegister(SPI_ADR_EnableBlock,0);
  if(value != 0)
  {
    Serial.println("Error");
    Serial.println(value, HEX);
    return counter; //1
  }
  value = readRegister(SPI_ADR_EnableBlock);
  if (  value != 0)
  {
    Serial.println("Error SPI_ADR_EnableBlock!");
  }
  ///////////////////////////////////////////////////////
  // writeSPI(02, 32'h0000_0001)
  counter = counter + 1;
  value = writeRegister(SPI_ADR_Thr,1);
  if(value != 0)
  {
    Serial.println("Error");
    return counter; //2
  }
  value = readRegister(SPI_ADR_Thr);
  if (  value != 1)
  {
    Serial.println("Error SPI_ADR_Thr!");
  }
  
  ///////////////////////////////////////////////////////
  // writeSPI(01, 32'h0000_0003);
  counter = counter + 1;
  value = writeRegister(SPI_ADR_PageNo,3);
  if(value != 0)
  {
    Serial.println("Error");
    return counter; //3
  }
  value = readRegister(SPI_ADR_PageNo);
  if (  value != 3)
  {
    Serial.println("Error SPI_ADR_PageNo!");
  }
  
  ///////////////////////////////////////////////////////
  // writeSPI(04, 32'h0000_0001);  
  counter = counter + 1;
  value = writeRegister(SPI_ADR_BistEnable,1);
  if(value != 0)
  {
    Serial.println("Error");
    return counter; //4
  }
  value = readRegister(SPI_ADR_BistEnable);
  if (  value != 1)
  {
    Serial.println("Error SPI_ADR_BistEnable!");
  }
  
  ///////////////////////////////////////////////////////
  // writeSPI(07, 32'h0000_0001);    
  counter = counter + 1;
  value = writeRegister(SPI_ADR_BistReset,1);
  if(value != 0)
  {
    Serial.println("Error");
    return counter; //5
  }
  value = readRegister(SPI_ADR_BistReset);
  if (  value != 1)
  {
    Serial.println("Error SPI_ADR_BistReset!");
  }
  
  ///////////////////////////////////////////////////////  
  // writeSPI(07, 32'h0000_0000); 
  counter = counter + 1;
  value = writeRegister(SPI_ADR_BistReset,0);
  if(value != 0)
  {
    Serial.println("Error");
    return counter; //6
  }
  value = readRegister(SPI_ADR_BistReset);
  if (  value != 0)
  {
    Serial.println("Error SPI_ADR_BistReset!");
  }
  
  ///////////////////////////////////////////////////////
  // writeSPI(01, 32'h0000_0000);   
  counter = counter + 1;
  value = writeRegister(SPI_ADR_PageNo,0);
  if(value != 0)
  {
    Serial.println("Error");
    return counter; //7
  }
  value = readRegister(SPI_ADR_PageNo);
  if (  value != 0)
  {
    Serial.println("Error SPI_ADR_PageNo!");
  }
  
  ///////////////////////////////////////////////////////
  // writeSPI(04, 32'h0000_0001); // enable block   
  counter = counter + 1;
  value = writeRegister(SPI_ADR_EnableBlock,1);
  if(value != 0)
  {
    Serial.println("Error");
    return counter; //8
  }
  value = readRegister(SPI_ADR_EnableBlock);
  if (  value != 1)
  {
    Serial.println("Error SPI_ADR_EnableBlock!");
  }
  
  ///////////////////////////////////////////////////////
  // start BIST
  // writeSPI(01, 32'h0000_0003);    
  counter = counter + 1;
  value = writeRegister(SPI_ADR_PageNo,3);
  if(value != 0)
  {
    Serial.println("Error");
    return counter; //9
  }
  value = readRegister(SPI_ADR_PageNo);
  if (  value != 3)
  {
    Serial.println("Error SPI_ADR_PageNo!");
  }
  
  ///////////////////////////////////////////////////////
  // writeSPI(05, 32'h0000_0001);  
  counter = counter + 1;
  value = writeRegister(SPI_ADR_BistStart,1);
  if(value != 0)
  {
    Serial.println("Error");
    return counter; //10
  }
  value = readRegister(SPI_ADR_BistStart);
  if (  value != 1)
  {
    Serial.println("Error SPI_ADR_BistStart!");
  }
  
  ///////////////////////////////////////////////////////   
  counter = counter + 1;
  
  delay(100);
  value = readRegister(SPI_ADR_BistDone);
  while (value != 0x1)
  {
    Serial.println("not yet done");
    //Serial.println(value, HEX);
    delay(100);
    value = readRegister(SPI_ADR_BistDone);
    //return counter; //11
  }
  
  Serial.println("BIST done");
  Serial.println(value, HEX);
  counter = counter + 1;
  value = readRegister(SPI_ADR_BistCRC);
 // Tested if I do actually get the same result reading more often -> which is fine!
 // Serial.println(value, HEX);
 // value = readRegister(SPI_ADR_BistCRC);
  return value;
}


void writeCheck()
{
  
  // take the chip select high to de-select:
  digitalWrite(chipSelectPin, HIGH);
  // default value of a register
  // we actually need to be on page 0, assume so for the first test
  uint32_t value = readRegister(SPI_ADR_DontCare); // has 32 bits that can be written and read!
  uint32_t randomnumber = random(0x7FFFFFFF);
  Serial.print("Randomize: ");
  Serial.println(randomnumber, HEX);
  writeRegister(SPI_ADR_DontCare,randomnumber);
  uint32_t result = readRegister(SPI_ADR_DontCare);
  Serial.print("Read value: ");
  Serial.println(result, HEX);
  if(result != randomnumber){
    Serial.println("Error!");
  }
  writeRegister(SPI_ADR_DontCare, value); //restoring default
  
  // take the chip select high to de-select:
  digitalWrite(chipSelectPin, HIGH);
  
}

int tb_SRAM()
{
  uint32_t value = readRegister(SPI_ADR_DeviceId);  // check SPI is working
  if(value != 0xA550C1A8)
  {
    Serial.println("Error");
    return -1;
  }
  value = writeRegister(SPI_ADR_PageNo,0);          // make sure we are on page 0
  value = writeRegister(SPI_ADR_State,1);           // write state
  value = writeRegister(SPI_ADR_EnableBlock, 0b100); // enable block C SRAM capable

          // pattern addr 0
	value = writeRegister(SPI_ADR_AmData0, 0x30000); // AM data 0
	value = writeRegister(SPI_ADR_AmData1, 0x30001); // AM data 1
	value = writeRegister(SPI_ADR_AmData2, 0x30002); // AM data 2
	value = writeRegister(SPI_ADR_AmData3, 0x30003); // AM data 3
	value = writeRegister(SPI_ADR_AmData4, 0x30004); // AM data 4
	value = writeRegister(SPI_ADR_AmData5, 0x30005); // AM data 5
	value = writeRegister(SPI_ADR_AmData6, 0x30006); // AM data 6
	value = writeRegister(SPI_ADR_AmData7, 0x30007); // AM data 7

	value = writeRegister(SPI_ADR_AmAddress, 0x2000); // AM addr 8192 (block C address 0)
	value = writeRegister(SPI_ADR_AmWeRe, 0b1); // Write
	value = writeRegister(SPI_ADR_AmWeRe, 0b0); // Write off

// pattern addr 1
	value = writeRegister(SPI_ADR_AmData0, 0x20000); // AM data 0
	value = writeRegister(SPI_ADR_AmData1, 0x20001); // AM data 1
	value = writeRegister(SPI_ADR_AmData2, 0x20002); // AM data 2
	value = writeRegister(SPI_ADR_AmData3, 0x20003); // AM data 3
	value = writeRegister(SPI_ADR_AmData4, 0x20004); // AM data 4
	value = writeRegister(SPI_ADR_AmData5, 0x20005); // AM data 5
	value = writeRegister(SPI_ADR_AmData6, 0x20006); // AM data 6
	value = writeRegister(SPI_ADR_AmData7, 0x20007); // AM data 7
	
	value = writeRegister(SPI_ADR_AmAddress, 0x2001); // AM addr 8192 (block C address 1)
	value = writeRegister(SPI_ADR_AmWeRe, 0b1); // Write
	value = writeRegister(SPI_ADR_AmWeRe, 0b0); // Write off

	// pattern addr 2
	value = writeRegister(SPI_ADR_AmData0, 0x40000); // AM data 0
	value = writeRegister(SPI_ADR_AmData1, 0x40001); // AM data 1
	value = writeRegister(SPI_ADR_AmData2, 0x40002); // AM data 2
	value = writeRegister(SPI_ADR_AmData3, 0x40003); // AM data 3
	value = writeRegister(SPI_ADR_AmData4, 0x40004); // AM data 4
	value = writeRegister(SPI_ADR_AmData5, 0x40005); // AM data 5
	value = writeRegister(SPI_ADR_AmData6, 0x40006); // AM data 6
	value = writeRegister(SPI_ADR_AmData7, 0x40007); // AM data 7
	
	value = writeRegister(SPI_ADR_AmAddress, 0x2002); // AM addr 8192 (block C address 1)
	value = writeRegister(SPI_ADR_AmWeRe, 0b1); // Write
	value = writeRegister(SPI_ADR_AmWeRe, 0b0); // Write off

	// read back
	value = writeRegister(SPI_ADR_State, 0b11);// read state
	value = writeRegister(SPI_ADR_AmAddress, 0x2000); // AM addr 8192 (block C address 0)
	value = writeRegister(SPI_ADR_AmWeRe, 0b1); // Read
	value = writeRegister(SPI_ADR_AmWeRe, 0b0); // Read off

	value = readRegister(0x20); // re-read reg_sram_data_0;
	Serial.println(value,HEX);
	if(value != 0x30000)
	{
		Serial.println("Error");
	}
	
	value = readRegister(0x21); // re-read reg_sram_data_1
	Serial.println(value,HEX);
	if(value != 0x30001)
	{
		Serial.println("Error");
	}
	
	value = readRegister(0x22); // re-read reg_sram_data_2
	Serial.println(value,HEX);
	if(value != 0x30002)
	{
		Serial.println("Error");
	}
	
	value = readRegister(0x23); // re-read reg_sram_data_3
	Serial.println(value,HEX);
	if(value != 0x30003)
	{
		Serial.println("Error");
	}
	
	value = readRegister(0x24); // re-read reg_sram_data_4
	Serial.println(value,HEX);
	if(value != 0x30004)
	{
		Serial.println("Error");
	}
	
	value = readRegister(0x25); // re-read reg_sram_data_5
	Serial.println(value,HEX);
	if(value != 0x30005)
	{
		Serial.println("Error");
	}
	
	value = readRegister(0x26); // re-read reg_sram_data_6
	Serial.println(value,HEX);
	if(value != 0x30006)
	{
		Serial.println("Error");
	}
	
	value = readRegister(0x27); // re-read reg_sram_data_7
	Serial.println(value,HEX);
	if(value != 0x30007)
	{
		Serial.println("Error");
	}
	return 0;
}

int writeAllRnd()
{
  // take the chip select high to de-select:
  digitalWrite(chipSelectPin, HIGH);
  
  uint32_t value = readRegister(SPI_ADR_DeviceId);  // check SPI is working
  if(value != 0xA550C1A8)
  {
    Serial.println("Error");
    return -1;
  }
  value = writeRegister(SPI_ADR_PageNo,0);          // make sure we are on page 0
  value = writeRegister(SPI_ADR_State,1);           // write state
  value = writeRegister(SPI_ADR_EnableBlock, 0b111); // enable all blocks
  uint32_t randomnumber;
  for (int addr = 0; addr < 0x3000; addr++)
  {
    if(addr % 0x400 == 0)
    {
      Serial.print("At addr: ");
      Serial.println(addr, HEX);
    }
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData0, randomnumber); // AM data 0
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData1, randomnumber); // AM data 1
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData2, randomnumber); // AM data 2
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData3, randomnumber); // AM data 3
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData4, randomnumber); // AM data 4
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData5, randomnumber); // AM data 5
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData6, randomnumber); // AM data 6
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData7, randomnumber); // AM data 7
  
    value = writeRegister(SPI_ADR_AmAddress, addr); // AM addr 8192 (block C address 0)
    value = writeRegister(SPI_ADR_AmWeRe, 0b1); // Write
    value = writeRegister(SPI_ADR_AmWeRe, 0b0); // Write off
  }
  
  value = writeRegister(SPI_ADR_State,0);           // back to idle state
  value = writeRegister(SPI_ADR_EnableBlock, 0);    // disable all blocks
  
  // take the chip select high to de-select:
  digitalWrite(chipSelectPin, HIGH);
  return 0;
}

void writeBlockSelect()
{
  Serial.println("-^-^-^-^-^-^-^-^-^-^-^-^-^-");
  Serial.println("Block?");
  int block;
  while(Serial.available() == 0){}
  block = Serial.parseInt();  
  block = writeBlock(block);
  if(block != 0)
  {
    Serial.print("ERROR: Block write failed");
    Serial.println(block, DEC);
  }
  Serial.println("-^-^-^-^-^-^-^-^-^-^-^-^-^-");
}

int writeBlock(int block)
{
  // take the chip select high to de-select:
  digitalWrite(chipSelectPin, HIGH);
  
  uint32_t value = readRegister(SPI_ADR_DeviceId);  // check SPI is working
  if(value != 0xA550C1A8)
  {
    Serial.println("Error");
    return -1;
  }
  if(block > 2 || block < 0)
  {
    return 1;
  }
  value = writeRegister(SPI_ADR_PageNo,0);          // make sure we are on page 0
  value = writeRegister(SPI_ADR_State,1);           // write state
  value = 0x1 << block;
  value = writeRegister(SPI_ADR_EnableBlock, value); // enable selected block
  uint32_t randomnumber;
  for (int addr = (block*0x1000); addr < ((block+1)*0x1000); addr++)
  {
    if(addr % 0x400 == 0)
    {
      Serial.print("At addr: ");
      Serial.println(addr, HEX);
    }
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData0, randomnumber); // AM data 0
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData1, randomnumber); // AM data 1
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData2, randomnumber); // AM data 2
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData3, randomnumber); // AM data 3
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData4, randomnumber); // AM data 4
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData5, randomnumber); // AM data 5
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData6, randomnumber); // AM data 6
    randomnumber = random(0x3FFFF);  
    value = writeRegister(SPI_ADR_AmData7, randomnumber); // AM data 7
  
    value = writeRegister(SPI_ADR_AmAddress, addr); // AM addr 8192 (block C address 0)
    value = writeRegister(SPI_ADR_AmWeRe, 0b1); // Write
    value = writeRegister(SPI_ADR_AmWeRe, 0b0); // Write off
  }
  
  value = writeRegister(SPI_ADR_State,0);           // back to idle state
  value = writeRegister(SPI_ADR_EnableBlock, 0);    // disable all blocks
  
  // take the chip select high to de-select:
  digitalWrite(chipSelectPin, HIGH);
  return 0;
}



int writeReadCoreC()
{
  int choice;
  while(Serial.available() == 0){}
  choice = Serial.parseInt();  
  
  uint32_t value = readRegister(SPI_ADR_DeviceId);  // check SPI is working
  if(value != 0xA550C1A8)
  {
    Serial.println("Error");
    return -1;
  }
  value = writeRegister(SPI_ADR_PageNo,0);          // make sure we are on page 0
  value = writeRegister(SPI_ADR_State,1);           // write state
  value = writeRegister(SPI_ADR_EnableBlock, 0b100); // enable block C SRAM capable

  uint32_t randomnumber;
  randomnumber = random(0x3CFFF);
  Serial.print("Our random number that is being added: ");
  Serial.println(randomnumber, HEX);
  int count_wrong_reads = 0;
  byte address;
  
  for (int addr = 0x2000; addr < 0x3000; addr++)
  {
    if(addr % 0x400 == 0)
    {
      Serial.print("Writing at addr: ");
      Serial.println(addr, HEX);
    }
    value = addr + randomnumber;  
    if(choice == 0)
    {
      value = 0;
    }
    if(choice == 1)
    {
      value = 0x3FFFF;
    }
    address = SPI_ADR_AmData0;
    for (int i = 0; i < 8; i++)
    {
      writeRegister(address, value);
      if(choice != 0 && choice != 1)
      {
        value = value + 1;
      }
      address++;
    }
    value = writeRegister(SPI_ADR_AmAddress, addr); // AM addr 8192 (block C address 0)
    value = writeRegister(SPI_ADR_AmWeRe, 0b1); // Write
    value = writeRegister(SPI_ADR_AmWeRe, 0b0); // Write off
  }

  value = writeRegister(SPI_ADR_State, 0b11);// read state
  
  for (int addr = 0x2000; addr < 0x3000; addr++)
  {
    if(addr % 0x400 == 0)
    {
      Serial.print("Reading at addr: ");
      Serial.println(addr, HEX);
    }
      // read back
    value = writeRegister(SPI_ADR_AmAddress, addr); // AM addr 8192 (block C address 0)
    value = writeRegister(SPI_ADR_AmWeRe, 0b1); // Read
    value = writeRegister(SPI_ADR_AmWeRe, 0b0); // Read off

    for (int i = 0x20; i < 0x28; i++)
    {
      value = readRegister(i); // re-read reg_sram_data_0;  
      if (choice == 0)
      {
        if (value != 0)
        {
          count_wrong_reads++;
/*          Serial.print("wrong at addr: ");
          Serial.print(addr, HEX);
          Serial.print(" in part: ");
          Serial.print((i-0x20), HEX);
          Serial.print(" value: ");
          Serial.print(value, HEX);
          Serial.print(" expected: ");
          Serial.println(0, HEX);*/
        }
      }
      if (choice == 1)
      {
        if (value != 0x3FFFF)
        {
          count_wrong_reads++;
/*          Serial.print("wrong at addr: ");
          Serial.print(addr, HEX);
          Serial.print(" in part: ");
          Serial.print((i-0x20), HEX);
          Serial.print(" value: ");
          Serial.print(value, HEX);
          Serial.print(" expected: ");
          Serial.println(0x3FFFF, HEX);*/
        }
      }   
      if (choice != 0 && choice != 1)
      {
        if(value != (addr+ (i-0x20) + randomnumber) )
        {
          count_wrong_reads++;
          /*Serial.print("wrong at addr: ");
          Serial.print(addr, HEX);
          Serial.print(" in part: ");
          Serial.print((i-0x20), HEX);
          Serial.print(" value: ");
          Serial.print(value, HEX);
          Serial.print(" expected: ");
          Serial.println((addr+ (i-0x20) + randomnumber), HEX);*/
        }
      }
    }
  }
  Serial.print("Result: wrong reads ");
  Serial.println(count_wrong_reads, DEC);

  Serial.println("Do you want to reread? (1 = yes, else = no)");
  while(Serial.available() == 0){}
  int reread = Serial.parseInt();  
  if(reread == 1)
  {
    count_wrong_reads = 0;
    for (int addr = 0x2000; addr < 0x3000; addr++)
    {
      if(addr % 0x400 == 0)
      {
        Serial.print("Reading at addr: ");
        Serial.println(addr, HEX);
      }
        // read back
      value = writeRegister(SPI_ADR_AmAddress, addr); // AM addr 8192 (block C address 0)
      value = writeRegister(SPI_ADR_AmWeRe, 0b1); // Read
      value = writeRegister(SPI_ADR_AmWeRe, 0b0); // Read off
  
      for (int i = 0x20; i < 0x28; i++)
      {
        value = readRegister(i); // re-read reg_sram_data_0;  
        if (choice == 0)
        {
          if (value != 0)
          {
            count_wrong_reads++;
//            Serial.print("wrong at addr: ");
//            Serial.print(addr, HEX);
//            Serial.print(" in part: ");
//            Serial.print((i-0x20), HEX);
//            Serial.print(" value: ");
//            Serial.print(value, HEX);
//            Serial.print(" expected: ");
//            Serial.println(0, HEX);
          }
        }
        if (choice == 1)
        {
          if (value != 0x3FFFF)
          {
            count_wrong_reads++;
//            Serial.print("wrong at addr: ");
//            Serial.print(addr, HEX);
//            Serial.print(" in part: ");
//            Serial.print((i-0x20), HEX);
//            Serial.print(" value: ");
//            Serial.print(value, HEX);
//            Serial.print(" expected: ");
//            Serial.println(0x3FFFF, HEX);
          }
        }   
        if (choice != 0 && choice != 1)
        {
          if(value != (addr+ (i-0x20) + randomnumber) )
          {
            count_wrong_reads++;
//            Serial.print("wrong at addr: ");
//            Serial.print(addr, HEX);
//            Serial.print(" in part: ");
//            Serial.print((i-0x20), HEX);
//            Serial.print(" value: ");
//            Serial.print(value, HEX);
//            Serial.print(" expected: ");
//            Serial.println((addr+ (i-0x20) + randomnumber), HEX);
          }
        }
      }
    }
  }
  
  value = writeRegister(SPI_ADR_State,0);           // back to idle state
  value = writeRegister(SPI_ADR_EnableBlock, 0);    // disable all blocks
  
  Serial.print("Result: wrong reads ");
  Serial.println(count_wrong_reads, DEC);
  return count_wrong_reads;
}
