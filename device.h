#ifndef DEVICE_H
#define DEVICE_H

// General purpose digital pin numbers on Arduino UNO
//const int spi_sckPin       = 13;
//const int spi_misoPin      = 12;
//const int spi_mosiPin      = 11;
const int ss_slavePin      = 10;
const int chipSelectPin    = 9; 
const int chipHoldInPin    = 8;  
const int chipHoldOutPin   = 7;  
const int chipIsSyncPin    = 6; 
const int chipResetPin     = 5; 

// SPI pin numbers on Arduino UNO
// Pins:  MOSI: 11
//        MISO: 12
//        SCK:  13
//        SS:   10 (slave -> set as output!)
//        SS:   use any pin! (master) for controlling AM
// Be aware: voltage level is actually 5V TTL
// inputs are fine with 3.3 V from ADA78
// output: ADG3304SRUZ only accepts max.: Digital Inputs (Y) −0.3 V to (VCCY + 0.3 V)
// source: https://www.mouser.de/datasheet/2/609/ADG3304-1517480.pdf so we need lvlshift 
// resistor network fine on breadboard
// use 500 Ohm and 1 kOhm for all outputs from Arduino towards ADA78

// Analog inputs
const int voltage1V0Pin = A0;
const int voltage1V8Pin = A1;
const int current1V0Pin = A2;
const int current1V8Pin = A3;  
// Arduino UNO: 5 V
const int voltageRange = 5000;  // in mV
const int voltageUnits = 1023;  // 10 bits

#endif // DEVICE_H
